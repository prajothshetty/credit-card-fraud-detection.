#For succesfull implementaion of this particular project,it is advised to make a virtual environment and install the required dependencies which are being listed in the requirements file respectively.


*The problem statement revolves around the banking domain in particular regarding to credit card fraud detection.

*AIM: 
To Identify fraudulent credit card transactions.

*Source of Data:
The dataset has been collected and analysed during a research collaboration of Worldline and the Machine Learning Group (http://mlg.ulb.ac.be) of ULB (Université Libre de Bruxelles) on big data mining and fraud detection. More details on current and past projects on related topics are available on https://www.researchgate.net/project/Fraud-detection-5 and the page of the DefeatFraud project.

*About the datasets:
The datasets contains transactions made by credit cards in September 2013 by european cardholders. This dataset presents transactions that occurred in the span of two days respectively.
It contains only numerical input variables which are the result of a PCA transformation.This is a sample of real world data,which is confidential so all of these are being masked off.
The features we have are:Features V1, V2, ... V28 are the principal components obtained with PCA, the only features which have not been transformed with PCA are 'Time' and 'Amount'. Feature 'Time' contains the seconds elapsed between each transaction and the first transaction in the dataset. The feature 'Amount' is the transaction Amount, this feature can be used for example-dependant cost-senstive learning.
Feature 'Class' is the response variable and it takes value 1 in case of fraud and 0 otherwise.